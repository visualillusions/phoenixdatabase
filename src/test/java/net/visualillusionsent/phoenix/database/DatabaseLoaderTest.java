/*
 * Copyright (c) 2016 Visual Illusions Entertainment
 * All rights reserved.
 */

package net.visualillusionsent.phoenix.database;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by darkdiplomat on 28-Aug-16.
 */
public class DatabaseLoaderTest {

    @Test
    public void testDatabaseLoad() {

        DatabaseLoader.doScanAndLoad();
        Assert.assertNotNull(Database.get("dummy"));

    }
}
