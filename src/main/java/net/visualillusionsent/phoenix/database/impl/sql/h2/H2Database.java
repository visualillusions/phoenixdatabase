package net.visualillusionsent.phoenix.database.impl.sql.h2;

import net.visualillusionsent.phoenix.database.DatabaseAccessException;
import net.visualillusionsent.phoenix.database.DatabaseReadException;
import net.visualillusionsent.phoenix.database.DatabaseTableInconsistencyException;
import net.visualillusionsent.phoenix.database.DatabaseWriteException;
import net.visualillusionsent.phoenix.database.column.Column;
import net.visualillusionsent.phoenix.database.column.ColumnType;
import net.visualillusionsent.phoenix.database.column.DataType;
import net.visualillusionsent.phoenix.database.impl.sql.JdbcConnectionManager;
import net.visualillusionsent.phoenix.database.impl.sql.SQLDatabase;
import net.visualillusionsent.phoenix.database.template.DataAccess;

import java.sql.*;
import java.util.*;

/**
 * Represents access to a H2 database
 *
 * @author Chris Ksoll (damagefilter)
 * @author Aaron (somners)
 * @author Jason Jones (darkdiplomat)
 */
public final class H2Database extends SQLDatabase {

    private static H2Database instance = new H2Database();
    private final String NULL_STRING = "NULL";

    // H2 should only have 1 connection to the file at any given time.
    private Connection h2Connection;

    private H2Database() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException cnfex) {
            throw new ExceptionInInitializerError("Failed to initialize the H2 driver");
        }
        JdbcConnectionManager.shutdown(); // Doesn't need to be running
    }

    public static H2Database getInstance() {
        return instance;
    }

    void testConnection() {
        getH2Connection();
    }

    private Connection getH2Connection() {
        try {
            if (h2Connection == null || h2Connection.isClosed()) {
                h2Connection = DriverManager.getConnection(getConnectionSyntax());
            }
        } catch (SQLException sqlex) {
            throw new RuntimeException("Failure acquiring H2 Database connection. " + sqlex.getMessage());
        }
        return h2Connection;
    }

    @Override
    public void insert(DataAccess data) throws DatabaseWriteException {
        if (this.doesEntryExist(data)) {
            return;
        }
        Connection conn = getH2Connection();
        PreparedStatement ps = null;

        try {
            StringBuilder fields = new StringBuilder();
            StringBuilder values = new StringBuilder();
            HashMap<Column, Object> columns = data.toDatabaseEntryList();
            Iterator<Column> it = columns.keySet().iterator();

            Column column;
            while (it.hasNext()) {
                column = it.next();
                if (!column.autoIncrement()) {
                    fields.append(column.columnName()).append(",");
                    values.append("?").append(",");
                }
            }
            if (fields.length() > 0) {
                fields.deleteCharAt(fields.length() - 1);
            }
            if (values.length() > 0) {
                values.deleteCharAt(values.length() - 1);
            }
            ps = conn.prepareStatement("INSERT INTO " + data.getName() + "(" + fields.toString() + ") VALUES(" + values.toString() + ")");

            int i = 1;
            for (Column c : columns.keySet()) {
                if (!c.autoIncrement()) {
                    setToStatement(i, columns.get(c), ps, c);
                    i++;
                }
            }

            if (ps.executeUpdate() == 0) {
                throw new DatabaseWriteException("Error inserting H2: no rows updated!");
            }
        } catch (SQLException | DatabaseTableInconsistencyException ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            close(conn, ps, null);
        }
    }

    @Override
    public void insertAll(List<DataAccess> data) throws DatabaseWriteException {
        for (DataAccess da : data) {
            insert(da);
        }
    }

    @Override
    public void update(DataAccess data, Map<String, Object> filters) throws DatabaseWriteException {
        if (!this.doesEntryExist(data)) {
            return;
        }
        Connection conn = getH2Connection();
        ResultSet rs = null;

        try {
            rs = this.getResultSet(conn, data, filters, true);
            if (rs != null) {
                if (rs.next()) {
                    HashMap<Column, Object> columns = data.toDatabaseEntryList();
                    Iterator<Column> it = columns.keySet().iterator();
                    Column column;
                    while (it.hasNext()) {
                        column = it.next();
                        if (column.isList()) {
                            rs.updateObject(column.columnName(), this.getString((List<?>) columns.get(column), column.listDelimiter()));
                        }
                        else {
                            rs.updateObject(column.columnName(), columns.get(column));
                        }
                    }
                    rs.updateRow();
                }
                else {
                    throw new DatabaseWriteException("Error updating DataAccess to H2, no such entry: " + data.toString());
                }
            }
        } catch (SQLException | DatabaseTableInconsistencyException | DatabaseReadException ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            PreparedStatement st = null;
            try {
                st = rs != null && rs.getStatement() instanceof PreparedStatement ? (PreparedStatement) rs.getStatement() : null;
            } catch (SQLException e) {
                log.error(e.getMessage(), e);
            }
            close(conn, st, rs);
        }
    }

    @Override
    public void updateAll(DataAccess template, Map<DataAccess, Map<String, Object>> list) throws DatabaseWriteException {
        // FIXME: Might be worthwhile collecting all queries into one statement?
        // But then if something errors out it's hard to find what it was
        for (DataAccess da : list.keySet()) {
            update(da, list.get(da));
        }
    }

    @Override
    public void remove(DataAccess dataAccess, Map<String, Object> filters) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        PreparedStatement ps = null;

        try {
            if (filters.size() > 0) {
                StringBuilder sb = new StringBuilder();
                Object[] fieldNames = filters.keySet().toArray();
                for (int i = 0; i < fieldNames.length && i < fieldNames.length; i++) {
                    sb.append(fieldNames[i]);
                    if (i + 1 < fieldNames.length) {
                        sb.append("=? AND ");
                    } else {
                        sb.append("=?");
                    }
                }

                ps = conn.prepareStatement("DELETE FROM " + dataAccess.getName() + " WHERE " + sb.toString() + "LIMIT 1");
                for (int i = 0; i < fieldNames.length && i < fieldNames.length; i++) {
                    String fieldName = String.valueOf(fieldNames[i]);
                    Column col = dataAccess.getColumnForName(fieldName);
                    if (col == null) {
                        throw new DatabaseReadException("Error deleting H2 row in " + dataAccess.getName() + ". Column " + fieldNames[i] + " does not exist!");
                    }
                    setToStatement(i + 1, filters.get(fieldName), ps, col);
                }

                if (ps.executeUpdate() == 0) {
                    throw new DatabaseWriteException("Error removing from H2: no rows updated!");
                }
            }
        } catch (DatabaseReadException | SQLException dre) {
            log.error(dre.getMessage(), dre);
        } finally {
            close(conn, ps, null);
        }
    }

    @Override
    public void removeAll(DataAccess dataAccess, Map<String, Object> filters) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        PreparedStatement ps = null;

        try {
            if (filters.size() > 0) {
                StringBuilder sb = new StringBuilder();
                Object[] fieldNames = filters.keySet().toArray();
                for (int i = 0; i < fieldNames.length && i < fieldNames.length; i++) {
                    sb.append("`").append(fieldNames[i]);
                    if (i + 1 < fieldNames.length) {
                        sb.append("`=? AND ");
                    } else {
                        sb.append("`=?");
                    }
                }

                ps = conn.prepareStatement("DELETE FROM " + dataAccess.getName() + " WHERE " + sb.toString());
                for (int i = 0; i < fieldNames.length && i < fieldNames.length; i++) {
                    String fieldName = String.valueOf(fieldNames[i]);
                    Column col = dataAccess.getColumnForName(fieldName);
                    if (col == null) {
                        throw new DatabaseReadException("Error deleting H2 row in " + dataAccess.getName() + ". Column " + fieldNames[i] + " does not exist!");
                    }
                    setToStatement(i + 1, filters.get(fieldName), ps, col);
                }

                if (ps.executeUpdate() == 0) {
                    throw new DatabaseWriteException("Error removing from H2: no rows updated!");
                }
            }
        } catch (DatabaseReadException | SQLException dre) {
            log.error(dre.getMessage(), dre);
        } finally {
            close(conn, ps, null);
        }
    }

    @Override
    public void load(DataAccess da, Map<String, Object> filters) throws DatabaseReadException {
        ResultSet rs = null;
        Connection conn = getH2Connection();
        HashMap<String, Object> dataSet = new HashMap<String, Object>();
        try {
            rs = this.getResultSet(conn, da, filters, true);
            if (rs != null) {
                if (rs.next()) {
                    for (Column column : da.getTableLayout()) {
                        if (column.isList()) {
                            dataSet.put(column.columnName(), this.getList(column.dataType(), rs.getString(column.columnName()), column.listDelimiter()));
                        } else if (rs.getObject(column.columnName()) instanceof Boolean) {
                            dataSet.put(column.columnName(), rs.getBoolean(column.columnName()));
                        }
                        else {
                            dataSet.put(column.columnName(), rs.getObject(column.columnName()));
                        }
                    }
                    da.load(dataSet);
                }
            }
        } catch (SQLException | DatabaseAccessException | DatabaseTableInconsistencyException dre) {
            log.error(dre.getMessage(), dre);
        } finally {
            try {
                PreparedStatement st = rs != null && rs.getStatement() instanceof PreparedStatement ? (PreparedStatement) rs.getStatement() : null;
                close(conn, st, rs);
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void loadAll(DataAccess typeTemplate, List<DataAccess> datasets, Map<String, Object> filters) throws DatabaseReadException {
        ResultSet rs = null;
        Connection conn = getH2Connection();
        List<HashMap<String, Object>> stuff = new ArrayList<HashMap<String, Object>>();
        try {
            rs = this.getResultSet(conn, typeTemplate, filters, false);
            if (rs != null) {
                while (rs.next()) {
                    HashMap<String, Object> dataSet = new HashMap<String, Object>();
                    for (Column column : typeTemplate.getTableLayout()) {
                        if (column.isList()) {
                            dataSet.put(column.columnName(), this.getList(column.dataType(), rs.getString(column.columnName()), column.listDelimiter()));
                        } else if (rs.getObject(column.columnName()) instanceof Boolean) {
                            dataSet.put(column.columnName(), rs.getBoolean(column.columnName()));
                        }
                        else {
                            dataSet.put(column.columnName(), rs.getObject(column.columnName()));
                        }
                    }
                    stuff.add(dataSet);
                }
            }
        } catch (DatabaseReadException | SQLException | DatabaseTableInconsistencyException dre) {
            log.error(dre.getMessage(), dre);
        } finally {
            try {
                PreparedStatement st = rs != null && rs.getStatement() instanceof PreparedStatement ? (PreparedStatement) rs.getStatement() : null;
                close(conn, st, rs);
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        try {
            for (HashMap<String, Object> temp : stuff) {
                DataAccess newData = typeTemplate.getInstance();
                newData.load(temp);
                datasets.add(newData);
            }
        } catch (DatabaseAccessException dae) {
            log.error(dae.getMessage(), dae);
        }
    }

    @Override
    public void updateSchema(DataAccess schemaTemplate) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        ResultSet rs = null;

        try {
            // First check if the table exists, if it doesn't we'll skip the rest
            // of this method since we're creating it fresh.
            DatabaseMetaData metadata = conn.getMetaData();
            rs = metadata.getTables(null, null, schemaTemplate.getName(), null);
            if (!rs.first()) {
                this.createTable(schemaTemplate);
            }
            else {

                LinkedList<String> toRemove = new LinkedList<String>();
                HashMap<String, Column> toAdd = new HashMap<String, Column>();
                Iterator<Column> it = schemaTemplate.getTableLayout().iterator();

                // TODO: Should update primary keys ...
                Column column;
                while (it.hasNext()) {
                    column = it.next();
                    toAdd.put(column.columnName(), column);
                }

                for (String col : this.getColumnNames(schemaTemplate)) {
                    if (!toAdd.containsKey(col)) {
                        toRemove.add(col);
                    }
                    else {
                        toAdd.remove(col);
                    }
                }

                for (String name : toRemove) {
                    this.deleteColumn(schemaTemplate.getName(), name);
                }
                for (Map.Entry<String, Column> entry : toAdd.entrySet()) {
                    try {
                        this.insertColumn(schemaTemplate.getName(), entry.getValue(), schemaTemplate.getClass().getField(entry.getValue().columnName()).get(schemaTemplate));
                    } catch (IllegalAccessException | NoSuchFieldException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                }
            }
        } catch (SQLException sqle) {
            throw new DatabaseWriteException("Error updating H2 schema: " + sqle.getMessage());
        } catch (DatabaseTableInconsistencyException dtie) {
            log.error("Error updating H2 schema." + dtie.getMessage(), dtie);
        } finally {
            close(conn, null, rs);
        }
    }

    @Override
    public String getConnectionSyntax() {
        return "jdbc:h2:./db/h2/" + dbCfg.getDatabaseName() + ";MV_STORE=FALSE;MVCC=FALSE";
    }

    @Override
    public void createTable(DataAccess data) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        PreparedStatement ps = null;

        try {
            StringBuilder fields = new StringBuilder();
            HashMap<Column, Object> columns = data.toDatabaseEntryList();
            Iterator<Column> it = columns.keySet().iterator();
            List<String> primary = new ArrayList<String>(2);

            Column column;
            while (it.hasNext()) {
                column = it.next();
                fields.append(column.columnName()).append(" ");
                fields.append(this.getDataTypeSyntax(column.dataType()));
                try {
                    Object defVal = data.getClass().getField(column.columnName()).get(data);
                    if (defVal != null) {
                        fields.append(" DEFAULT '").append(defVal.toString()).append("'");
                    }
                } catch (IllegalAccessException | NoSuchFieldException e) {
                    // OOPS
                }
                if (column.notNull()) {
                    fields.append(" NOT NULL");
                }
                if (column.autoIncrement()) {
                    fields.append(" AUTO_INCREMENT");
                }
                if (column.columnType().equals(ColumnType.PRIMARY)) {
                    primary.add(column.columnName());
                }
                if (it.hasNext()) {
                    fields.append(", ");
                }
            }
            ps = conn.prepareStatement("CREATE TABLE IF NOT EXISTS " + data.getName() + "(" + fields.toString() + ")");
            ps.execute();
        } catch (SQLException ex) {
            throw new DatabaseWriteException("Error creating H2 table '" + data.getName() + "'. " + ex.getMessage());
        } catch (DatabaseTableInconsistencyException ex) {
            log.error(ex.getMessage() + " Error creating H2 table '" + data.getName() + "'. ", ex);
        } finally {
            close(conn, ps, null);
        }
    }

    @Override
    public void insertColumn(String tableName, Column column, Object defVal) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        PreparedStatement ps = null;

        try {
            if (column != null && !column.columnName().trim().equals("")) {
                ps = conn.prepareStatement("ALTER TABLE " + tableName + " ADD " + column.columnName() + " "
                        + this.getDataTypeSyntax(column.dataType())
                        + (column.notNull() ? " NOT NULL" : "")
                        + (defVal != null ? " DEFAULT " + defVal.toString() : "")
                                          );
                ps.execute();
            }
        } catch (SQLException ex) {
            throw new DatabaseWriteException("Error adding H2 column: " + column.columnName(), ex);
        } finally {
            close(conn, ps, null);
        }
    }

    @Override
    public void deleteColumn(String tableName, String columnName) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        PreparedStatement ps = null;

        try {
            if (columnName != null && !columnName.trim().equals("")) {
                ps = conn.prepareStatement("ALTER TABLE " + tableName + " DROP " + columnName);
                ps.execute();
            }
        } catch (SQLException ex) {
            throw new DatabaseWriteException("Error deleting H2 column: " + columnName);
        } finally {
            close(conn, ps, null);
        }
    }

    @Override
    public boolean doesEntryExist(DataAccess data) throws DatabaseWriteException {
        Connection conn = getH2Connection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean toRet = false;

        try {
            StringBuilder sb = new StringBuilder();
            HashMap<Column, Object> columns = data.toDatabaseEntryList();
            Iterator<Column> it = columns.keySet().iterator();

            Column column;
            while (it.hasNext()) {
                column = it.next();
                if (!column.autoIncrement()) {
                    Object o = columns.get(column);
                    if (o == null) {
                        continue;
                    }
                    if (sb.length() > 0) {
                        sb.append(" AND ").append(column.columnName());
                    }
                    else {
                        sb.append(column.columnName());
                    }
                    sb.append(" = ?");
                    // if (it.hasNext()) {
                    // sb.append("' = ? AND ");
                    // } else {
                    // sb.append("' = ?");
                    // }
                }
            }
            ps = conn.prepareStatement("SELECT * FROM " + data.getName() + " WHERE " + sb.toString());
            it = columns.keySet().iterator();

            int index = 1;

            while (it.hasNext()) {
                column = it.next();
                if (!column.autoIncrement()) {
                    Object o = columns.get(column);
                    if (o == null) {
                        continue;
                    }
                    setToStatement(index, columns.get(column), ps, column);
                    index++;
                }
            }
            rs = ps.executeQuery();
            if (rs != null) {
                toRet = rs.next();
            }
        } catch (SQLException ex) {
            throw new DatabaseWriteException(ex.getMessage() + " Error checking H2 Entry Key in "
                    + data.toString()
            );
        } catch (DatabaseTableInconsistencyException ex) {
            log.error("", ex);
        } finally {
            close(conn, ps, rs);
        }
        return toRet;
    }

    @Override
    public ResultSet getResultSet(Connection conn, DataAccess data, Map<String, Object> filters, boolean limitOne) throws DatabaseReadException {
        PreparedStatement ps;
        ResultSet toRet;

        try {

            if (filters.size() > 0) {
                StringBuilder sb = new StringBuilder();
                Object[] fieldNames = filters.keySet().toArray();
                for (int i = 0; i < fieldNames.length && i < fieldNames.length; i++) {
                    sb.append("`").append(fieldNames[i]);
                    if (i + 1 < fieldNames.length) {
                        sb.append("`=? AND ");
                    }
                    else {
                        sb.append("`=?");
                    }
                }
                if (limitOne) {
                    sb.append(" LIMIT 1");
                }
                ps = conn.prepareStatement("SELECT * FROM " + data.getName() + " WHERE " + sb.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                for (int i = 0; i < fieldNames.length && i < fieldNames.length; i++) {
                    String fieldName = String.valueOf(fieldNames[i]);
                    Column col = data.getColumnForName(fieldName);
                    if (col == null) {
                        throw new DatabaseReadException("Error fetching H2 ResultSet in " + data.getName() + ". Column " + fieldNames[i] + " does not exist!");
                    }
                    setToStatement(i + 1, filters.get(fieldName), ps, col);
                }
            }
            else {
                if (limitOne) {
                    ps = conn.prepareStatement("SELECT * FROM " + data.getName() + " LIMIT 1", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                }
                else {
                    ps = conn.prepareStatement("SELECT * FROM " + data.getName(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                }
            }
            toRet = ps.executeQuery();
        } catch (Exception ex) {
            throw new DatabaseReadException("Error fetching H2 ResultSet in " + data.getName(), ex);
        }

        return toRet;
    }

    @Override
    public List<String> getColumnNames(DataAccess data) {
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<String> columns = new ArrayList<String>();
        String columnName;

        Connection connection = getH2Connection();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SHOW COLUMNS FROM " + data.getName());
            while (resultSet.next()) {
                columnName = resultSet.getString("field");
                columns.add(columnName);
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                }
            }
            close(connection, null, resultSet);
        }
        return columns;
    }

    @Override
    public String getDataTypeSyntax(DataType type) {
        switch (type) {
            case BYTE:
                return "TINYINT";
            case INTEGER:
                return "INT";
            case FLOAT:
                return "FLOAT";
            case DOUBLE:
                return "DOUBLE";
            case LONG:
                return "BIGINT";
            case SHORT:
                return "SMALLINT";
            case STRING:
                return "LONGVARCHAR";
            case BOOLEAN:
                return "BOOLEAN";
        }
        return "";
    }

    /**
     * Sets the given object as the given type to the given index
     * of the given PreparedStatement.
     *
     * @param index
     *         the index to set to
     * @param o
     *         the object to set
     * @param ps
     *         the prepared statement
     * @param t
     *         the DataType hint
     *
     * @throws DatabaseWriteException
     *         when an SQLException was raised or when the data type doesn't match the objects type
     */
    private void setToStatement(int index, Object o, PreparedStatement ps, Column t) throws DatabaseWriteException {
        try {
            if (t.isList()) {
                ps.setString(index, getString((List<?>) o, t.listDelimiter()));
            } else {
                switch (t.dataType()) {
                    case BYTE:
                        ps.setByte(index, (Byte) o);
                        break;
                    case INTEGER:
                        ps.setInt(index, (Integer) o);
                        break;
                    case FLOAT:
                        ps.setFloat(index, (Float) o);
                        break;
                    case DOUBLE:
                        ps.setDouble(index, (Double) o);
                        break;
                    case LONG:
                        ps.setLong(index, (Long) o);
                        break;
                    case SHORT:
                        ps.setShort(index, (Short) o);
                        break;
                    case STRING:
                        ps.setString(index, (String) o);
                        break;
                    case BOOLEAN:
                        ps.setBoolean(index, (Boolean) o);
                        break;
                }
            }
        } catch (SQLException | ClassCastException ex) {
            throw new DatabaseWriteException("Failed to set property to prepared statement!", ex);
        }
    }

    /**
     * Gets a Java List representation from the H2 String.
     *
     * @param type
     * @param field
     *
     * @return
     */
    private List<Comparable<?>> getList(DataType type, String field, String delimiter) {
        List<Comparable<?>> list = new ArrayList<Comparable<?>>();
        if (field == null) {
            return list;
        }
        switch (type) {
            case BYTE:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Byte.valueOf(s));
                }
                break;
            case INTEGER:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Integer.valueOf(s));
                }
                break;
            case FLOAT:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Float.valueOf(s));
                }
                break;
            case DOUBLE:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Double.valueOf(s));
                }
                break;
            case LONG:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Long.valueOf(s));
                }
                break;
            case SHORT:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Short.valueOf(s));
                }
                break;
            case STRING:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(s);
                }
                break;
            case BOOLEAN:
                for (String s : field.split(delimiter)) {
                    if (s.equals(NULL_STRING)) {
                        list.add(null);
                        continue;
                    }
                    list.add(Boolean.valueOf(s));
                }
                break;
        }
        return list;
    }

    /**
     * Get the database entry for a Java List.
     *
     * @param list
     *
     * @return a string representation of the passed list.
     */
    public String getString(List<?> list, String delimiter) {
        if (list == null) {
            return NULL_STRING;
        }
        StringBuilder sb = new StringBuilder();
        for (Object o : list) {
            if (o == null) {
                sb.append(NULL_STRING);
            }
            else {
                sb.append(String.valueOf(o));
            }
            sb.append(delimiter);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * Close a set of working data.
     * This will return all the data to the connection pool.
     * You can pass null for objects that are not relevant in your given context
     *
     * @param c  the connection object
     * @param ps the prepared statement
     * @param rs the result set
     */
    private void close(Connection c, PreparedStatement ps, ResultSet rs) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (c != null) {
                c.close();
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }
}
