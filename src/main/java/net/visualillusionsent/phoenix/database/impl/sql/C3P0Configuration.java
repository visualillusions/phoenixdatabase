/*
 * Copyright (c) 2016 Visual Illusions Entertainment
 * All rights reserved.
 */

package net.visualillusionsent.phoenix.database.impl.sql;

import net.visualillusionsent.utils.PropertiesFile;

import java.io.File;

import static net.visualillusionsent.phoenix.database.Database.log;

/**
 * c3p0 configuration manager
 *
 * @author Jason Jones (darkdiplomat)
 */
public final class C3P0Configuration {

    private PropertiesFile c3p0cfg;

    public C3P0Configuration(String path) {
        File test = new File(path);

        if (!test.exists()) {
            log.debug("Could not find the c3p0 configuration at " + path + ", creating default.");
        }

        this.c3p0cfg = new PropertiesFile(path);
        verifyConfig();
    }

    /**
     * Reloads the configuration file
     */
    public void reload() {
        c3p0cfg.reload();
        verifyConfig();
    }

    /**
     * Get the configuration file
     */
    PropertiesFile getFile() {
        return c3p0cfg;
    }

    /**
     * Creates the default configuration
     */
    private void verifyConfig() {
        if (c3p0cfg.getHeaderLines().isEmpty()) {
            c3p0cfg.addHeaderLines(
                    "For more settings explanations see following websites ...",
                    "http://javatech.org/2007/11/c3p0-connectionpool-configuration-rules-of-thumb/",
                    "https://community.jboss.org/wiki/HowToConfigureTheC3P0ConnectionPool?_sscc=t"
                                  );
        }

        // c3p0 SQL settings
        c3p0cfg.getInt("acquire-increment", 5);
        c3p0cfg.setComments("acquire-increment", "Determines how many connections at a time c3p0 will try to acquire when the pool is exhausted.");

        c3p0cfg.getInt("max-connection-idle-time", 900); //15 minutes
        c3p0cfg.setComments("max-connection-idle-time", "Determines how long idle connections can stay in the connection pool before they are removed.");

        c3p0cfg.getInt("max-excess-connections-idle-time", 1800); // 30 minutes
        c3p0cfg.setComments("max-excess-connections-idle-time", "Time until the connection pool will be culled down to min-connection-pool-size. Set 0 to not enforce pool shrinking.");

        c3p0cfg.getInt("max-connection-pool-size", 10);
        c3p0cfg.setComments("max-connection-pool-size", "The maximum allowed number of pooled connections. More for larger servers");

        c3p0cfg.getInt("min-connection-pool-size", 3);
        c3p0cfg.setComments("min-connection-pool-size", "The minimum amount of connections allowed. More means more memory usage but takes away some impact from creating new connections.");

        c3p0cfg.getInt("num-helper-threads", 4);
        c3p0cfg.setComments("num-helper-threads", "Amount of threads that will perform slow JDBC operations (closing idle connections, returning connections to pool etc)");

        c3p0cfg.getInt("return-connection-timeout", 900); //15 minutes
        c3p0cfg.setComments("return-connection-timeout", "Defines a time a connection can remain checked out. After that it will be forced back into the connection pool.");

        c3p0cfg.getInt("connection-test-frequency", 0); // 60 minutes
        c3p0cfg.setComments("idle-connection-test-frequency", "Every this amount of seconds idle connections will be checked for validity. Set 0 to turn off");

        c3p0cfg.getInt("max-cached-statements", 50);
        c3p0cfg.setComments("max-cached-statements", "Number of max cached statements on all connections. (Roughly 5 * expected pooled connections)");

        c3p0cfg.getInt("max-statements-per-connection", 5);
        c3p0cfg.setComments("max-statements-per-connection", "Number of max cached statements on a single connection.");

        c3p0cfg.getInt("statement-cache-close-threads", 1);
        c3p0cfg.setComments("statement-cache-close-threads", "Number of threads to use when closing statements is deferred (happens when parent connection is still in use)");

        c3p0cfg.save();
    }

    /**
     * Get the maximum number of concurrent connections to the database.
     * This might be null if the datasource is not a connection oriented database type such as XML.
     *
     * @return database maximum connections
     */
    public int getDatabaseMaxConnections() {
        return c3p0cfg.getInt("maxConnections");
    }

    /**
     * Defines the total number PreparedStatements a DataSource will cache.
     * The pool will destroy the least-recently-used PreparedStatement when it hits this limit.
     *
     * @return config for max cached statements
     */
    public int getMaxCachedStatements() {
        return c3p0cfg.getInt("max-cached-statements", 50);
    }

    /**
     * Defines how many statements each pooled Connection is allowed to own.
     * You can set this to a bit more than the number of PreparedStatements
     * your application frequently uses, to avoid churning.
     *
     * @return config for max num of pooled statements per connection
     */
    public int getMaxCachedStatementsPerConnection() {
        return c3p0cfg.getInt("max-statements-per-connection", 5);
    }

    /**
     * If greater than zero, the Statement pool will defer physically close()ing cached Statements
     * until its parent Connection is not in use by any client or internally (in e.g. a test) by the pool itself.
     *
     * @return config num of threads used to defer closing statements
     */
    public int getNumStatementCloseThreads() {
        return c3p0cfg.getInt("statement-cache-close-threads", 1);
    }

    /**
     * Defines the interval of checking validity of pooled connections in seconds.
     *
     * @return connection re-check interval
     */
    public int getConnectionTestFrequency() {
        return c3p0cfg.getInt("connection-test-frequency", 3600);
    }

    /**
     * Defines the time in seconds a connection can stay checked out, before it is returned to the connection pool.
     *
     * @return num of seconds a connection can stay checked out
     */
    public int getReturnConnectionTimeout() {
        return c3p0cfg.getInt("return-connection-timeout", 900);
    }

    /**
     * Defines the amount of threads to use when executing slow JDBC operations,
     * such as closing connections and statements.
     *
     * @return num of threads to use for heavy JDBC operations
     */
    public int getNumHelperThreads() {
        return c3p0cfg.getInt("num-helper-threads", 4);
    }

    /**
     * Defines the minimum amount of connections to keep alive in the connection pool.
     *
     * @return min amount of connections
     */
    public int getMinPoolSize() {
        return c3p0cfg.getInt("min-connection-pool-size", 3);
    }

    /**
     * Defines the maximum allowed number of connections in the connection pool.
     *
     * @return max allowed connections in pool
     */
    public int getMaxPoolSize() {
        return c3p0cfg.getInt("max-connection-pool-size", 10);
    }

    /**
     * Number of seconds that Connections in excess of minPoolSize
     * should be permitted to remain idle in the pool before being culled.
     * Set 0 to turn off culling
     *
     * @return seconds to keep excess connections
     */
    public int getMaxExcessConnectionsIdleTime() {
        return c3p0cfg.getInt("max-excess-connections-idle-time", 1800);
    }

    /**
     * Determines how many connections at a time to acquire when the pool is exhausted.
     *
     * @return connections to acquire
     */
    public int getAcquireIncrement() {
        return c3p0cfg.getInt("acquire-increment", 5);
    }

    /**
     * Time to keep idle connections in the pool before they are closed and discarded.
     *
     * @return keep-alive time of connections in pool
     */
    public int getMaxConnectionIdleTime() {
        return c3p0cfg.getInt("max-connection-idle-time", 900);
    }
}
