package net.visualillusionsent.phoenix.database.impl.sql;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import net.visualillusionsent.phoenix.database.Database;
import net.visualillusionsent.phoenix.database.DatabaseAccessException;
import net.visualillusionsent.phoenix.database.DatabaseConfiguration;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import static net.visualillusionsent.phoenix.database.Database.log;

/**
 * Represents a connection (pool) manager for all sorts of JDBC connections.
 * In our particular case that is mysql.
 */
public class JdbcConnectionManager {

    // The data source pool ;)
    private ComboPooledDataSource cpds;
    private SQLType sqlType;
    private static JdbcConnectionManager instance;

    /**
     * Instantiates the connection manager
     *
     * @param sqlType
     *         the database type
     *
     * @throws java.sql.SQLException
     */
    private JdbcConnectionManager(SQLType sqlType) throws SQLException {
        DatabaseConfiguration cfg = Database.dbCfg;
        C3P0Configuration c3p0 = Database.c3poCfg;
        cpds = new ComboPooledDataSource();
        this.sqlType = sqlType;
        try {
            cpds.setDriverClass(sqlType.getClassPath());
            cpds.setJdbcUrl(cfg.getDatabaseUrl(sqlType.getIdentifier()));
            cpds.setUser(cfg.getDatabaseUser());
            cpds.setPassword(cfg.getDatabasePassword());

            // For settings explanations see
            // http://javatech.org/2007/11/c3p0-connectionpool-configuration-rules-of-thumb/
            // https://community.jboss.org/wiki/HowToConfigureTheC3P0ConnectionPool?_sscc=t

            //connection pooling
            cpds.setAcquireIncrement(c3p0.getAcquireIncrement());
            cpds.setMaxIdleTime(c3p0.getMaxConnectionIdleTime());
            cpds.setMaxIdleTimeExcessConnections(c3p0.getMaxExcessConnectionsIdleTime());
            cpds.setMaxPoolSize(c3p0.getMaxPoolSize());
            cpds.setMinPoolSize(c3p0.getMinPoolSize());
            cpds.setNumHelperThreads(c3p0.getNumHelperThreads());
            cpds.setUnreturnedConnectionTimeout(c3p0.getReturnConnectionTimeout());
            cpds.setIdleConnectionTestPeriod(c3p0.getConnectionTestFrequency());

            //Statement pooling
            cpds.setMaxStatements(c3p0.getMaxCachedStatements());
            cpds.setMaxStatementsPerConnection(c3p0.getMaxCachedStatementsPerConnection());
            cpds.setStatementCacheNumDeferredCloseThreads(c3p0.getNumStatementCloseThreads());
        }
        catch (PropertyVetoException e) {
            log.error("Failed to configure the connection pool!", e);
        }
        //Test connection...
        //If this fails it throws an SQLException so we're notified
        Connection c = cpds.getConnection();
        c.close();
    }

    /**
     * Get the Database type.
     *
     * @return the type
     */
    public SQLType getSQLType() {
        return this.sqlType;
    }

    /**
     * Create a new instance of this connection manager.
     *
     * @return an instance of the manager
     *
     * @throws DatabaseAccessException
     */
    private static JdbcConnectionManager getInstance() throws DatabaseAccessException {
        if (instance == null) {
            try {
                SQLType t = SQLType.forName(Database.dbCfg.getSource());
                if (t == null) {
                    throw new DatabaseAccessException("Given data source is not a valid JDBC Database type");
                }
                instance = new JdbcConnectionManager(t);
            }
            catch (SQLException e) {
                throw new DatabaseAccessException("Unable to instantiate Connection Pool!", e);
            }
        }
        return instance;
    }

    /**
     * Get a connection form the connection pool.
     *
     * @return connection from the pool
     */
    public static Connection getConnection() {
        JdbcConnectionManager cman = getInstance();
        try {
            return cman.cpds.getConnection();
        } catch (SQLException sqlex) {
            throw new DatabaseAccessException("Unable to gain connection to SQL DB", sqlex);
        }
    }

    /**
     * Shut down the connection pool.
     * Should be called when the system is reloaded or goes down to prevent data loss.
     */
    public static void shutdown() {
        if (instance == null) {
            // already shut down or never instantiated (perhaps because we're running on a non-jdbc database)
            return;
        }
        instance.cpds.close();
        instance = null;
    }
}
