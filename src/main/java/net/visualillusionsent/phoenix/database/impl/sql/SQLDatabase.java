package net.visualillusionsent.phoenix.database.impl.sql;

import net.visualillusionsent.phoenix.database.Database;
import net.visualillusionsent.phoenix.database.DatabaseReadException;
import net.visualillusionsent.phoenix.database.column.DataType;
import net.visualillusionsent.phoenix.database.template.DataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Map;

/**
 * SQL Database specification
 *
 * @author Jason Jones (darkdiplomat)
 */
public abstract class SQLDatabase extends Database {

    public abstract ResultSet getResultSet(Connection conn, DataAccess data, Map<String, Object> filters, boolean limitOne) throws DatabaseReadException;

    public abstract String getDataTypeSyntax(DataType type);

}
