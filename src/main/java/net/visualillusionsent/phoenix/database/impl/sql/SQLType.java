package net.visualillusionsent.phoenix.database.impl.sql;

import com.google.common.collect.Maps;

import java.util.HashMap;

/**
 * Helper type so we can easily identify the driver type further down the code
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public final class SQLType {
    //TODO: Integrate special connection handling and JdbcConnectionManager disabling

    private static final HashMap<DriverContainer, SQLType> driverRegistry = Maps.newHashMap();
    private final DriverContainer container;

    private SQLType(DriverContainer container) {
        this.container = container;
    }

    private static class DriverContainer {
        public final String classpath;
        public final String identifier;

        private DriverContainer(String identifier, String classpath) {
            this.classpath = classpath;
            this.identifier = identifier;
        }

        public final boolean equals(Object obj) {
            return obj instanceof DriverContainer &&
                    ((DriverContainer) obj).classpath.equals(this.classpath) &&
                    ((DriverContainer) obj).identifier.equals(this.identifier);
        }
    }


    public String getClassPath() {
        return this.container.classpath;
    }

    public String getIdentifier() {
        return this.container.identifier;
    }

    public static SQLType forName(String name) {
        for (SQLType t : driverRegistry.values()) {
            if (t.getIdentifier().equalsIgnoreCase(name)) {
                return t;
            }
        }
        return null;
    }

    public static SQLType registerSQLDriver(String identifier, String classpath) {
        DriverContainer temp = new DriverContainer(identifier, classpath);
        if (!driverRegistry.containsKey(temp)) {
            SQLType newType = new SQLType(temp);
            driverRegistry.put(temp, newType);
            return newType;
        }
        return null;
    }

    static {
        registerSQLDriver("mysql", "com.mysql.jdbc.Driver");
    }
}