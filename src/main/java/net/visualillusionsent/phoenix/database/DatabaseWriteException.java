package net.visualillusionsent.phoenix.database;

/**
 * Thrown when trying to write data to the database and there is an error
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public class DatabaseWriteException extends DatabaseAccessException {

    private static final long serialVersionUID = 20150130103711L;

    public DatabaseWriteException() {
    }

    public DatabaseWriteException(String msg) {
        super(msg);
    }

    public DatabaseWriteException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DatabaseWriteException(Throwable cause) {
        super(cause);
    }
}
