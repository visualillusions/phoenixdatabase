package net.visualillusionsent.phoenix.database;

import net.visualillusionsent.utils.PropertiesFile;
import net.visualillusionsent.utils.UtilityException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import static net.visualillusionsent.phoenix.database.Database.log;

/**
 * Loads external database extensions
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
final class DatabaseLoader {

    private DatabaseLoader() {
        // No reason to make an instance of this class
    }

    /**
     * Scans database folder, loads all valid databases and registers them
     * at Database.Type. This must be the first bootstrapping step,
     * as all other steps require a functional database.
     * This also means this must not make use of anything that isn't loaded already
     */
    static void doScanAndLoad() {
        File dbFolder = new File("database_extensions/");
        if (!dbFolder.exists()) {
            if (!dbFolder.mkdirs()) {
                log.error("Unable to create database extensions directory");
                return;
            }
        }

        File[] extensions = dbFolder.listFiles();
        if (extensions != null) {
            for (File extension : extensions) {
                if (!extension.getName().endsWith(".jar")) {
                    continue; // Not a valid extension
                }

                try {
                    PropertiesFile inf = new PropertiesFile(extension.getAbsolutePath(), "phoenix_database.phx");

                    URLClassLoader loader = new URLClassLoader(new URL[]{extension.toURI().toURL()}, DatabaseLoader.class.getClassLoader());

                    String mainclass = inf.getString("main-class");
                    String dbName = inf.getString("database-name");
                    Class<?> dbClass = loader.loadClass(mainclass);

                    Database db = (Database) dbClass.newInstance();
                    if (db != null) {
                        DataSource.registerDatabase(dbName, db);
                    }
                }
                catch (UtilityException uex) {
                    log.error("Could not find Database implementation class", uex);
                }
                catch (ClassNotFoundException cnfex) {
                    log.error("Could not find Database implementation class", cnfex);
                }
                catch (IllegalAccessException iaex) {
                    log.error("Could not initialize Database implementation", iaex);
                }
                catch (DatabaseException dbex) {
                    log.error("Could not initialize Database implementation", dbex);
                }
                catch (SecurityException sex) {
                    log.error("Could not initialize Database implementation", sex);
                }
                catch (InstantiationException iex) {
                    log.error("Could not initialize Database implementation", iex);
                }
                catch (MalformedURLException murlex) {
                    log.error("Could not initialize Database implementation", murlex);
                }
            }
        }
    }
}
