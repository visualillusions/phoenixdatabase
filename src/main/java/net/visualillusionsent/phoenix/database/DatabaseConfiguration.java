package net.visualillusionsent.phoenix.database;

import net.visualillusionsent.utils.PropertiesFile;

import java.io.File;

import static net.visualillusionsent.phoenix.database.Database.log;

/**
 * Database Configuration settings
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jason (darkdiplomat)
 */
public final class DatabaseConfiguration {
    private PropertiesFile cfg;

    public DatabaseConfiguration(String path) {
        File test = new File(path);

        if (!test.exists()) {
            log.info("Could not find the database configuration at " + path + ", creating default.");
        }
        this.cfg = new PropertiesFile(path);
        verifyConfig();
    }

    /**
     * Reloads the configuration file
     */
    public void reload() {
        cfg.reload();
        verifyConfig();
    }

    /**
     * Get the configuration file
     */
    PropertiesFile getFile() {
        return cfg;
    }

    /**
     * Creates the default configuration
     */
    private void verifyConfig() {
        cfg.getString("db-source", "h2");
        cfg.setComments("db-source", "The Datasource Type, by default H2 is used");

        cfg.getString("db-name", "phoenixDB");
        cfg.getString("db-name", "The name of the database used to contain the data");

        /* SQL Settings */
        cfg.getString("sql-host", "localhost");
        cfg.getString("sql-username", "sa");
        cfg.getString("sql-password", "");
        cfg.getInt("sql-port", 0);
        cfg.getInt("sql-maxConnections", 1);

        cfg.save();
    }

    public String getSource() {
        return cfg.getString("db-source", "h2");
    }

    /**
     * Get the URL to the database.
     * This is a combination of host, port and database
     *
     * @param driver
     *         the JDBC driver name (ie: mysql or h2)
     *
     * @return database url
     */
    public String getDatabaseUrl(String driver) {
        return Database.get(driver).getConnectionSyntax();
    }

    /**
     * Get the database host, defaulting to localhost
     *
     * @return database host
     */
    public String getDatabaseHost() {
        return cfg.getString("sql-host", "localhost");
    }

    /**
     * Get the database port
     *
     * @return The configured port or 0
     */
    public int getDatabasePort() {
        return cfg.getInt("sql-port", 0);
    }

    /**
     * Get the name of the database. Defaults to 'phoenix_database'
     *
     * @return database name
     */
    public String getDatabaseName() {
        return cfg.getString("db-name", "phoenix_database");
    }

    /**
     * Get database user
     * This might be null if the datasource is not a password protected database type such as XML.
     *
     * @return database username
     */
    public String getDatabaseUser() {
        return cfg.getString("sql-username");
    }

    /**
     * Get database password.
     * This might be null if the datasource is not a password protected database type such as XML.
     *
     * @return database password
     */
    public String getDatabasePassword() {
        return cfg.getString("sql-password");
    }
}
