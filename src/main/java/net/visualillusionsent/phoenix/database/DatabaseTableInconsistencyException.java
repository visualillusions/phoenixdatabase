package net.visualillusionsent.phoenix.database;

/**
 * Thrown when data is inconsistent, this often occurs when you make a mistake in your @Column fields.
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public class DatabaseTableInconsistencyException extends DatabaseException {

    private static final long serialVersionUID = 20150130103625L;

    public DatabaseTableInconsistencyException() {
    }

    public DatabaseTableInconsistencyException(String msg) {
        super(msg);
    }

    public DatabaseTableInconsistencyException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DatabaseTableInconsistencyException(Throwable cause) {
        super(cause);
    }
}
