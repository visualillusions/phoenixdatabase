package net.visualillusionsent.phoenix.database;

import java.util.HashMap;

import static net.visualillusionsent.phoenix.database.Database.log;

/**
 * The Database Source
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public final class DataSource {
    private static HashMap<String, Database> registeredDatabases = new HashMap<String, Database>();

    public static void registerDatabase(String name, Database db) throws DatabaseException {

        if (registeredDatabases.containsKey(name)) {
            throw new DatabaseException(name + " cannot be registered. Type already exists");
        }

        registeredDatabases.put(name, db);
        log.info(String.format("Registered %s Database", name));
    }

    public static Database getDatabaseFromType(String name) {
        return registeredDatabases.get(name);
    }
}
