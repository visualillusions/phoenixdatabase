package net.visualillusionsent.phoenix.database.column;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a field of a DataAccess object as a column in a database table.
 * This annotation also describes the column so that it will be handled properly in the database
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {

    /**
     * This field is dynamically overridden to match the field name its attached to
     */
    String columnName() default "";

    DataType dataType();

    int dataLength() default 0;

    ColumnType columnType() default ColumnType.NORMAL;

    /**
     * Should we auto-increment the value of this field?
     */
    boolean autoIncrement() default false;

    /**
     * Is this field an implementation of the List interface?
     */
    boolean isList() default false;

    /**
     * The character used as a delimiter to serialize  a list if applicable
     * Default: ¶ (Pilcrow Sign, u00B6)
     */
    String listDelimiter() default "\u00B6";

    boolean notNull() default false;
}
