package net.visualillusionsent.phoenix.database.column;

/**
 * Data Typing
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public enum DataType {
    INTEGER(Integer.class),
    FLOAT(Float.class),
    DOUBLE(Double.class),
    LONG(Long.class),
    SHORT(Short.class),
    BYTE(Byte.class),
    STRING(String.class),
    BOOLEAN(Boolean.class);

    private Class<?> cls;

    DataType(Class<?> cls) {
        this.cls = cls;
    }

    public boolean isAssignable(Class<?> cls) {
        return this.cls.isAssignableFrom(cls);
    }

    public static DataType fromString(String in) {
        for (DataType t : DataType.values()) {
            if (in.equalsIgnoreCase(t.name())) {
                return t;
            }
        }
        return STRING;
    }

    public Class<?> getTypeClass() {
        return cls;
    }
}