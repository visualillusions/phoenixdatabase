package net.visualillusionsent.phoenix.database.column;

/**
 * Column Type
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public enum ColumnType {
    UNIQUE,
    PRIMARY,
    NORMAL
}
