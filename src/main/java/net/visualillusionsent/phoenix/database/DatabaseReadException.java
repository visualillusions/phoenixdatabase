package net.visualillusionsent.phoenix.database;

/**
 * Thrown when trying to read data from the database and there is an error
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public class DatabaseReadException extends DatabaseAccessException {

    private static final long serialVersionUID = 20150130103642L;

    public DatabaseReadException() {
    }

    public DatabaseReadException(String msg) {
        super(msg);
    }

    public DatabaseReadException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DatabaseReadException(Throwable cause) {
        super(cause);
    }
}
