package net.visualillusionsent.phoenix.database;

/**
 * Thrown when there is an error accessing the database
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public class DatabaseAccessException extends DatabaseException {

    private static final long serialVersionUID = 20150130103345L;

    public DatabaseAccessException() {
    }

    public DatabaseAccessException(String msg) {
        super(msg);
    }

    public DatabaseAccessException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DatabaseAccessException(Throwable cause) {
        super(cause);
    }
}
