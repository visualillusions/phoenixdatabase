package net.visualillusionsent.phoenix.database;

/**
 * Thrown when trying to register an already registered database
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public class DatabaseException extends RuntimeException {

    private static final long serialVersionUID = 20150130103158L;

    public DatabaseException() {
    }

    public DatabaseException(String msg) {
        super(msg);
    }

    public DatabaseException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }
}
