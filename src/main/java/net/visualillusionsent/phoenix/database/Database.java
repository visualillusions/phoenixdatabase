package net.visualillusionsent.phoenix.database;

import net.visualillusionsent.phoenix.database.column.Column;
import net.visualillusionsent.phoenix.database.impl.sql.C3P0Configuration;
import net.visualillusionsent.phoenix.database.impl.sql.h2.H2Database;
import net.visualillusionsent.phoenix.database.impl.sql.mysql.MySQLDatabase;
import net.visualillusionsent.phoenix.database.template.DataAccess;

import java.util.List;
import java.util.Map;

/**
 * A database representation, used to store any kind of data
 *
 * @author Chris Ksoll (damagefilter)
 * @author Jason Jones (darkdiplomat)
 */
public abstract class Database {
    public static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("Phoenix-Database");
    public static final DatabaseConfiguration dbCfg = new DatabaseConfiguration("config/database.cfg");
    public static final C3P0Configuration c3poCfg = new C3P0Configuration("config/sql_connman.cfg");

    public static void initialize() {
        // The moment this class requires initialization, the database libraries should be loaded
        DataSource.registerDatabase("h2", H2Database.getInstance());
        DataSource.registerDatabase("mysql", MySQLDatabase.getInstance());
        DatabaseLoader.doScanAndLoad();
    }

    /**
     * Returns the Database instance configured in the DatabaseConfiguration
     *
     * @return configured database
     */
    public static Database get() {
        String source = dbCfg.getSource();
        Database database = DataSource.getDatabaseFromType(source);
        if (database != null) {
            return database;
        }
        else {
            log.warn("DataSource '" + source + "' is not available!");
            return null;
        }
    }

    /**
     * Attempts to get a Database for a given source
     *
     * @param source
     *         the data source to get a database for
     *
     * @return the acquired database or {@code null} of not database of the give source is available
     */
    public static Database get(String source) {
        return DataSource.getDatabaseFromType(source);
    }

    /**
     * Insert the given DataAccess object as new set of data into database
     *
     * @param data
     *         the data to insert
     *
     * @throws DatabaseWriteException
     *         when something went wrong during the write operation
     */
    public abstract void insert(DataAccess data) throws DatabaseWriteException;

    /**
     * Insert a range of DataAccess objects at once.
     *
     * @param data
     *         the list of data to insert
     *
     * @throws DatabaseWriteException
     *         when something went wrong during the write operation
     */
    public abstract void insertAll(List<DataAccess> data) throws DatabaseWriteException;

    /**
     * Updates the record in the database that fits to your fields and values given.
     * Those are NOT the values and fields to update. Those are values and fields to identify
     * the correct entry in the database to update. The updated data must be provided in the DataAccess
     *
     * @param data
     *         the data to be updated. Additionally this acts as information about the table schema
     * @param filters
     *         FieldName->Value map to filter which rows should be updated
     *
     * @throws DatabaseWriteException
     */
    public abstract void update(DataAccess data, Map<String, Object> filters) throws DatabaseWriteException;

    /**
     * Updates the records in the database that fits to your fields and values given.
     * Those are NOT the values and fields to update. Those are values and fields to identify
     * the correct entry in the database to update. The updated data must be provided in the DataAccess
     *
     * @param template
     *         the template data access used for verification.
     * @param list
     *         a map of data access objects to insert and the filters to apply them with
     *
     * @throws DatabaseWriteException
     */
    public abstract void updateAll(DataAccess template, Map<DataAccess, Map<String, Object>> list) throws DatabaseWriteException;

    /**
     * Removes the data set from the given table that suits the given field names and values.
     *
     * @param da
     *         the DataAccess object that specifies the data that should be removed
     * @param filters
     *         FieldName->Value map to filter which rows should be deleted
     *
     * @throws DatabaseWriteException
     */
    public abstract void remove(DataAccess da, Map<String, Object> filters) throws DatabaseWriteException;

    /**
     * Removes the data set from the given table that suits the given field names and values.
     *
     * @param da
     *         the DataAccess object that specifies the data that should be removed
     * @param filters
     *         FieldName->Value map to filter which rows should be deleted
     *
     * @throws DatabaseWriteException
     */
    public abstract void removeAll(DataAccess da, Map<String, Object> filters) throws DatabaseWriteException;

    /**
     * This method will fill your DataAccess object with the first data set from database,
     * that matches the given values in the given fields.<br>
     * For instance if you pass String[] {"score", "name"}<br>
     * with respective values Object[] {130, "damagefilter"},<br>
     * PhoenixDatabase will look in the database for records where score=130 and name=damagefilter.<br>
     * PhoenixDatabase will only look in the table with the name you have set in your AccessObject<br>
     *
     * @param dataset
     *         The class of your DataAccess object
     * @param filters
     *         FieldName->Value map to filter which row should be loaded
     *
     * @throws DatabaseReadException
     */
    public abstract void load(DataAccess dataset, Map<String, Object> filters) throws DatabaseReadException;

    /**
     * Loads all results that match the field - values given into a DataAccess list.
     *
     * @param typeTemplate
     *         The type template (an instance of the dataaccess type you want to load)
     * @param datasets
     *         DataAccess set - you can safely cast those to the type of typeTemplate
     * @param filters
     *         FieldName->Value map to filter which rows should be loaded
     *
     * @throws DatabaseReadException
     */
    public abstract void loadAll(DataAccess typeTemplate, List<DataAccess> datasets, Map<String, Object> filters) throws DatabaseReadException;

    /**
     * Updates the database table fields for the given DataAccess object.
     * This method will remove fields that aren't there anymore and add new ones if applicable.
     *
     * @param schemaTemplate
     *         the new schema
     */
    public abstract void updateSchema(DataAccess schemaTemplate) throws DatabaseWriteException;

    public abstract void createTable(DataAccess data) throws DatabaseWriteException;

    public abstract void insertColumn(String tableName, Column column, Object defVal) throws DatabaseWriteException;

    public abstract void deleteColumn(String tableName, String columnName) throws DatabaseWriteException;

    public abstract boolean doesEntryExist(DataAccess data) throws DatabaseWriteException;

    public abstract List<String> getColumnNames(DataAccess data);

    /**
     * Gets the syntax for correct connecting to the database
     * ie: jdbc:h2:~/test
     *
     * @return connection syntax
     */
    public abstract String getConnectionSyntax();
}
