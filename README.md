Phoenix Database
=========

[![Build Status](https://ci.visualillusionsent.net/buildStatus/icon?job=PhoenixDatabase)](https://ci.visualillusionsent.net/job/PhoenixDatabase/)
[Latest Build](https://ci.visualillusionsent.net/job/PhoenixDatabase/lastBuild/)
[Latest Successful Build](https://ci.visualillusionsent.net/job/PhoenixDatabase/lastSuccessfulBuild/)

PhoenixDatabase is a Database management API based on the initial work of The CanaryMod Team.

Development
=============

Maven
-------------

        <dependency>
            <groupId>net.visualillusionsent</groupId>
            <artifactId>phoenix-database</artifactId>
            <version>[1.0.0-SNAPSHOT,)</version> <!-- Will always use the latest version found (including snapshots)-->
        </dependency>


*Repository:*

        <repository>
            <id>vi-repo</id>
            <name>Visual Illusions Repository</name>
            <url>http://repo.visualillusionsent.net/repository/public/</url>
        </repository>


Non-Maven
-------------

Good luck

Java Docs
-------------

Not yet available

Pull Requests
=============

It helps us when others take the time to submit fixes rather than just pointing out bugs/inconsistencies.  
However, We have standards for the sources we have. Things like formatting  
and generally following the [Sun/Oracle coding standards](http://www.oracle.com/technetwork/java/javase/documentation/codeconvtoc-136057.html)  
We also ask that you read and sign our Contributor License Agreement Form  (TODO)

Source Formatting and requirements
-------------

* No tabs; use 4 spaces instead.
* No trailing whitespaces.
* No CRLF line endings, LF only.
  * Git can handle this by auto-converting CRLF line endings into LF when you commit, and vice versa when it checks out code onto your filesystem.
    You can turn on this functionality with the core.autocrlf setting.
    If you’re on a Windows machine, set it to true — this converts LF endings into CRLF when you check out code. (git config --global core.autocrlf true)
  * Eclipse: http://stackoverflow.com/a/11596227/532590
  * NetBeans: http://stackoverflow.com/a/1866385/532590
  * IntelliJ: http://stackoverflow.com/a/9872584
* JavaDocs well written (as necessary)
* Matching how we format statements

        public class MyClass { //note the whitespace
            public void function() {
                if (something) {
                    // do stuff
                }
                else if (somethingElse) {
                    // do other stuff
                }
                else {
                    // do else stuff
                }
            }
        }

Issues
-------

When reporting issues, be sure to include details on how to reproduce the issue and details on what the issue involves.
If you would like a feature added, feel free to open an issue describing the feature you would like added and how it could be useful.